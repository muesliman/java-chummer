package com.shadow.model;

import com.shadow.model.gear.ComplexForm;
import com.shadow.model.gear.Connection;

import java.util.Arrays;

/**
 * Created by felix on 01.02.18.
 */
public class Runner {
    private String name;
    private String alias;
    private int metatyp;
    private int age;
    private int sex;
    private int size;
    private int weight;
    private String eyecolor;
    private String haircolor;
    private String skincolor;
    //0 = kontostand, 1= Karma ges, 2 = Karma akt, 3 = Strassenruf, 4 = Schlechter Ruf, 5 = prominenz;
    private int[] konto_Karma_ruf;
    //1 - 16
    private int[] attr;
    private Triple[] fertigkeiten;
    private Triple[] wissensfertigkeiten;
    private int[] gaben_handycaps;
    private Connection[] connections;
    private ComplexForm[] complexForms;

    public Runner(String name, int metatyp) {
        this.name = name;
        this.metatyp = metatyp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setMetatyp(int metatyp) {
        this.metatyp = metatyp;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setEyecolor(String eyecolor) {
        this.eyecolor = eyecolor;
    }

    public void setHaircolor(String haircolor) {
        this.haircolor = haircolor;
    }

    public void setSkincolor(String skincolor) {
        this.skincolor = skincolor;
    }

    public void setKonto_Karma_ruf(int[] konto_Karma_ruf) {
        this.konto_Karma_ruf = konto_Karma_ruf;
    }

    public void setAttr(int[] attr) {
        this.attr = attr;
    }

    public void setFertigkeiten(Triple[] fertigkeiten) {
        this.fertigkeiten = fertigkeiten;
    }

    public void setWissensfertigkeiten(Triple[] wissensfertigkeiten) {
        this.wissensfertigkeiten = wissensfertigkeiten;
    }

    public void setGaben_handycaps(int[] gaben_handycaps) {
        this.gaben_handycaps = gaben_handycaps;
    }

    public void setConnections(Connection[] connections) {
        this.connections = connections;
    }

    public void setComplexForms(ComplexForm[] complexForms) {
        this.complexForms = complexForms;
    }

    public String getName() {

        return name;
    }

    public String getAlias() {
        return alias;
    }

    public int getMetatyp() {
        return metatyp;
    }

    public int getAge() {
        return age;
    }

    public int getSex() {
        return sex;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public String getEyecolor() {
        return eyecolor;
    }

    public String getHaircolor() {
        return haircolor;
    }

    public String getSkincolor() {
        return skincolor;
    }

    public int[] getKonto_Karma_ruf() {
        return konto_Karma_ruf;
    }

    public int[] getAttr() {
        return attr;
    }

    public Triple[] getFertigkeiten() {
        return fertigkeiten;
    }

    public Triple[] getWissensfertigkeiten() {
        return wissensfertigkeiten;
    }

    public int[] getGaben_handycaps() {
        return gaben_handycaps;
    }

    public Connection[] getConnections() {
        return connections;
    }

    public ComplexForm[] getComplexForms() {
        return complexForms;
    }

    @Override
    public String toString() {
        return "Runner{" +
                "name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", metatyp=" + metatyp +
                ", age=" + age +
                ", sex=" + sex +
                ", size=" + size +
                ", weight=" + weight +
                ", eyecolor='" + eyecolor + '\'' +
                ", haircolor='" + haircolor + '\'' +
                ", skincolor='" + skincolor + '\'' +
                ", konto_Karma_ruf=" + Arrays.toString(konto_Karma_ruf) +
                ", attr=" + Arrays.toString(attr) +
                ", fertigkeiten=" + Arrays.toString(fertigkeiten) +
                ", wissensfertigkeiten=" + Arrays.toString(wissensfertigkeiten) +
                ", gaben_handycaps=" + Arrays.toString(gaben_handycaps) +
                ", connections=" + Arrays.toString(connections) +
                ", complexForms=" + Arrays.toString(complexForms) +
                '}';
    }
}

package com.shadow.model;

/**
 * Created by felix on 02.02.18.
 */
public class Triple<X, Y, Z> {
    public X x;
    public Y y;
    public Z z;
    public Triple(X x, Y y,Z z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

